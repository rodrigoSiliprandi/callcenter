import React from 'react';

import ListCall from "./ui/ListCall"
import RegisterCall from "./ui/RegisterCall"

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from "@react-navigation/stack"

const Stack = createStackNavigator()

const StackScreenOptions = {
  headerShowm: false
}

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="List Call" component={ListCall} options={{headerShown:false}} />
        <Stack.Screen name="Register Call" component={RegisterCall} options={{headerShown:false}}/>
      </Stack.Navigator>
    </ NavigationContainer>
  );
};
        

export default App;
